#ifndef __INCLUDE_H__
#define __INCLUDE_H__

#include  "common.h"
#include  "stdbool.h"
#include  "stdio.h"

#define BLU

/*
 * Include 用户自定义的头文件
 */
#include  "MK60_wdog.h"
#include  "MK60_gpio.h"     //IO口操作
#include  "MK60_uart.h"     //串口
#include  "MK60_SysTick.h"
#include  "MK60_lptmr.h"    //低功耗定时器(延时)
#include  "MK60_i2c.h"      //I2C
#include  "MK60_spi.h"      //SPI
#include  "MK60_ftm.h"      //FTM
#include  "MK60_pit.h"      //PIT
#include  "MK60_FLASH.h"    //FLASH
#include  "MK60_adc.h"

#include  "ISR.h"
#include  "computer.h"     //多功能调试助手
#include  "camera.h"       //摄像头总头文件
#include  "motor.h"
#include  "steer.h"
#include  "oled.h"
#include  "key.h"
#include  "encounter.h"
#include  "infrared.h"
#include  "control.h"
#include  "boma.h"
#include  "pid.h"
#include "oledDisplay.h"
#include "analysis.h"
#include "adxl345.h"
#include "UserFunction.h"
#include "VCAN_NRF24L0.h"

#endif