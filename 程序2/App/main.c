#include "include.h"
#include "UserFunction.h"

void  main(void)
{
/***************这段代码开启FN的浮点运算**********************************************************/
#if  MK60F15
    SCB->CPACR |=((3UL << 10*2)|(3UL << 11*2));     /* set CP10 and CP11 Full Access */
#endif
/************************************************************************************************/   
   DisableInterrupts;                            //禁止总中断     
/*************模块初始化***********/
   uart_init (UART5, 38400);
   uart_tx_irq_dis(UART5);
   uart_rx_irq_en (UART5);
   motor_init();
   steer_init();
   camera_init(imgbuff);
   OLED_Init();
   key_Init();                             //按键端口初始化
   encounter_init();                      //编码器定时器初始化
//   adxl345_init();                        //加速度传感器初始化
    
   gpio_init (PTB22, GPO,1);              
   pit_init_ms(PIT2, 5);                 //PID中断，用于PID计算
   lptmr_timing_ms(10);                  // LPTMR 定时 10ms,用于按键检测
    
    //配置中断服务函数
   set_vector_handler(PORTA_VECTORn , PORTA_IRQHandler);   //设置LPTMR的中断服务函数为 PORTA_IRQHandler
   set_vector_handler(DMA0_VECTORn , DMA0_IRQHandler);     //设置LPTMR的中断服务函数为 DMA0_IRQHandler
   set_vector_handler(PORTB_VECTORn , PORTB_IRQHandler);   //设置LPTMR的中断服务函数为 PORTB_IRQHandler
   set_vector_handler(LPTMR_VECTORn,lptmr_hander);         // 设置中断复位函数到中断向量表里
   set_vector_handler(PIT2_VECTORn ,PIT2_IRQHandler);
   set_vector_handler(PORTE_VECTORn ,PORTE_IRQHandler);    //设置 PORTE 的中断复位函数为 PORTE_VECTORn
#ifdef BLU
   set_vector_handler(UART5_RX_TX_VECTORn ,UART5_IRQHandler);
#endif
    
   enable_irq(LPTMR_IRQn);  
   enable_irq(PORTE_IRQn);
    
   NVIC_SetPriorityGrouping(0x06);                         //前一位抢断，后三位响应
   NVIC_SetPriority(PORTA_IRQn, 0010);                     //0号抢断，2号响应
   NVIC_EnableIRQ(PORTA_IRQn);
   
   NVIC_SetPriority(DMA0_IRQn, 0001);                      //0号抢断，1号响应
   NVIC_EnableIRQ(DMA0_IRQn);
   
   NVIC_SetPriority(PORTB_IRQn, 1111);                     //1号抢断，7号响应
   NVIC_EnableIRQ(PORTB_IRQn);
    
   NVIC_SetPriority(PIT2_IRQn, 1001);                     //1号抢断，1号响应
   NVIC_EnableIRQ(PIT2_IRQn);
    
   NVIC_SetPriority(LPTMR_IRQn, 1001);                     //1号抢断，1号响应
   NVIC_EnableIRQ(LPTMR_IRQn);
    
   gpio_set (PTB22, 0);
   EnableInterrupts;			                    //开总中断 
//   get_distance();
   DELAY_MS(100);

    while(1)
    {
       control_main();                                //主控程序
       oled_Display();                                //状态显示
    }
}

