#ifndef  __CAMERA_H
#define  __CAMERA_H

#define CAMERA_OV7725_EAGLE         2       //山外鹰眼

#define USE_CAMERA      CAMERA_OV7725_EAGLE   //选择使用的 摄像头


typedef struct
{
    uint8 addr;                 /*寄存器地址*/
    uint8 val;                   /*寄存器值*/
} reg_s;

//定义图像采集状态
typedef enum
{
    IMG_NOTINIT = 0,
    IMG_FINISH,             //图像采集完毕
    IMG_FAIL,               //图像采集失败(采集行数少了)
    IMG_GATHER,             //图像采集中
    IMG_START,              //开始采集图像
    IMG_STOP,               //禁止图像采集
} IMG_STATUS_e;


#include <stdbool.h>
#include  "SCCB.h"
#include  "ov7725.h"

extern uint8 imgbuff[CAMERA_SIZE];
extern bool photo[CAMERA_H][CAMERA_W];

extern void img_extract(void *dst, void *src, uint32_t srclen);


// 摄像头 接口统一改成 如下模式

//  camera_init(imgaddr);
//  camera_get_img();
//  camera_cfg(rag,val)


//  camera_vsync()  //场中断
//  camera_href()   //行中断
//  camera_dma()    //DMA中断

// 需要 提供 如下 宏定义
// #define  CAMERA_USE_HREF    1     //是否使用 行中断 (0 为 不使用，1为使用)
// #define  CAMERA_COLOR       1     //摄像头输出颜色 ， 0 为 黑白二值化图像 ，1 为 灰度 图像 ，2 为 RGB565 图像
// #define  CAMERA_POWER       0     //摄像头 电源选择， 0 为 3.3V ,1 为 5V



#endif


