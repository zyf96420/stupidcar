#ifndef  __ANALYSIS_H
#define  __ANALYSIS_H

#include "include.h"

typedef enum
{
    Filter_No = 0,
    Filter_Blur,             
    
    Filter_NumMax
} Filter_STATUS_e;

extern uint8 Target_X;
extern uint8 Target_Y;
extern uint8 Target_X_Last;
extern uint8 Target_X_Last_Last;
extern uint8 Target_Y_Last;
extern uint8 Target_Y_Last_Last;
extern uint16 Target_Width;
extern uint16 Target_Height;
extern uint16 Target_Width_Last;
extern uint16 Target_Height_Last;

void photo_unzip(uint8 *img);                           //解压图片方便处理
void photo_zip(uint8 *img);                             //压缩图片
void Image_Filter(uint8 Type);                          //无用
uint8 Image_Search_Target(uint8 *img,uint8 *X,uint8 *Y,uint16 *Width,uint16 *Height);//获取目标大致坐标(X 0-8) (Y 0-59)和大小(0-2550)
void Get_Target_Offset(uint8 *Point_Old,uint8 *Point_Now,uint16 *Distance);
void Get_Target_Offset_Y(uint8 Point_Old_Y,uint8 Point_Now_Y,char *Distance);          //获取目标当前Y坐标与前一次Y坐标的差值
void Get_Car_Turn_Speed(uint8 *X,int32 *Speed);

#endif



