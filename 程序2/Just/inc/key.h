#ifndef __KEY_H
#define __KEY_H

#include "include.h"

void key_Init(void);            //初始化按键端口
uint8 Get_Key1_State_Enter(void);//获得Enter键(pb19)状态,返回值1为按下，2为放开，3为长按，0为没按下
uint8 Get_Key2_State_Up(void);
uint8 Get_Key3_State_Down(void);

void OLED_Key(void);

#endif
