#ifndef __STEER_H
#define __STEER_H

#include "include.h"
#define steer_channel FTM_CH0

void steer_init(void);
void steer_turn(int8 degree);
void steer_reset(void);
void steer_pid_turn(void);

#endif