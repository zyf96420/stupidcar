#ifndef __ISR_H
#define __ISR_H

#include "include.h"

//定义oled状态
typedef enum
{
    null_mode=0,
    camera_mode,          //图像显示
    infrared_mode,        //超声波测距
    motor_mode,           //电机PWM
    pid_mode,             //pid
    sequence_mode,       //信标转弯路径选择
    run_mode,            //启动模式 
}oled_mode;


void PORTA_IRQHandler(void);
void DMA0_IRQHandler(void);
void PORTB_IRQHandler(void);
void PIT2_IRQHandler(void);
void lptmr_hander(void);
void PORTE_IRQHandler(void);
void UART5_IRQHandler(void);

extern uint8 key1_enter_flag;//切换键
extern uint8 key2_enter_flag;//增加键
extern uint8 key3_enter_flag;//减少键
extern uint8 key1_enter_flag_long;//切换键长按
extern int Spd;//期望速度
extern uint16 duzhuan_count;//堵转次数(每10ms加1)
extern int32 tick_10ms;

extern char Start_Count_Flag;

#endif