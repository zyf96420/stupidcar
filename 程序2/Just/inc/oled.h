#ifndef _OLED_H
#define _OLED_H
#include "include.h"

//OLED模式设置
//0:4线串行模式
//1:并行8080模式
#define OLED_MODE 0

//#define SIZE 12
#define XLevelL		0x00
#define XLevelH		0x10
#define XLevel		((XLevelH&0x0F)*16+XLevelL)
#define Max_Column	128
#define Max_Row		64
#define	Brightness	0xCF        //亮度调节
#define X_WIDTH 	128
#define Y_WIDTH 	64	  

//Oled引脚置位清零
#define OLED_CS_Set() 	gpio_set (PTD12,1)
#define OLED_CS_Clr() 	gpio_set (PTD12,0)
#define OLED_RST_Set() 	gpio_init (PTD14, GPO, 1)
#define OLED_RST_Clr() 	gpio_init (PTD14, GPO, 0)
#define OLED_DC_Set() 	gpio_set (PTD11,1)
#define OLED_DC_Clr() 	gpio_set (PTD11,0)
#define OLED_SCLK_Set() gpio_set (PTD15,1)//SCLK/D0
#define OLED_SCLK_Clr() gpio_set (PTD15,0)//SCLK/D0
#define OLED_SDIN_Set() gpio_set (PTD13,1)//SDIN/D1
#define OLED_SDIN_Clr() gpio_set (PTD13,0)//SDIN/D1

//变量类型定义
#define uint8 unsigned char
#define uint16 unsigned int
#define byte uint8
#define word uint16

//Oled命令字定义
#define OLED_CMD  0	//写命令
#define OLED_DATA 1	//写数据

//OLED函数

void OLED_Init(void);																										//OLED初始化函数
void OLED_WR_Byte(unsigned char dat,unsigned char cmd);	    						//OLED写字节函数
void OLED_Display_On(void);																							//OLED开显示函数
void OLED_Display_Off(void);																						//OLED关显示函数
void OLED_Delayms(unsigned int ms);																			//OLED延时函数
void OLED_Clear(void);																									//OLED清屏函数	
void OLED_DrawPoint(unsigned char x,unsigned char y,unsigned char t);		//OLED画点函数
void OLED_Fill(unsigned char x1,unsigned char y1,unsigned char x2,unsigned char y2,unsigned char dot);		//OLED填充函数
void OLED_ShowChar(unsigned char x,unsigned char y,unsigned char chr,unsigned size);  										//OLED字符显示函数
void OLED_Showfloat(unsigned char x,unsigned char y,double i,unsigned char size);
void OLED_ShowNum(unsigned char x,unsigned char y,unsigned int num,unsigned char len,unsigned char size);	//OLED数字显示函数
void OLED_ShowString(unsigned char x,unsigned char y, unsigned char *p,unsigned size);										//OLED字符串显示函数	 
void OLED_Set_Pos(unsigned char x, unsigned char y);																											//OLED设置光标位置	
void OLED_ShowCHinese(unsigned char x,unsigned char y,unsigned char no);																	//OLED中文显示函数
void OLED_ShowCHinese_FREESACLE(unsigned char x,unsigned char y,unsigned char no);												//OLED中文”飞思卡尔”显示函数
void OLED_ShowCHinese_JISUWOLIU(unsigned char x,unsigned char y,unsigned char no);										//OLED中文”海之精灵”显示函数
void OLED_ShowStart(void);																		//海之精灵启动界面
void OLED_ShowMain(unsigned char Choice);										  //海之精灵调试主界面显示
void OLED_ShowWait(unsigned char Choice);											//海之精灵调等待界面显示
void OLED_ShowCarSpeed(int Speed,int S,int K,char flag);
void OLED_ShowPIDValue(double Kp,double Ki,double Kd,char flag);
void OLED_ShowPDValue(double W_KP,double W_KD,double Z_KP,double Z_KD,char flag);
void OLED_ShowModeSet(char Flag);
//void OLED_ShowSR04(float distance);
void OLED_ShowSHARP_AD(uint8_t sharp_adc);
void OLED_ShowSHARP_ADSET(uint8_t sharp_adc);
void OLED_Show_X_SET(float K_CROSSROAD,float K_HILL,uint8_t crossroads_rejust,char flag);
void OLED_ShowCarSpeed2(int Speed,int S,char flag);
void OLED_Show_Start_Delay(uint32_t Start_Delay);

void LCD_Rectangle(byte x1,byte y1,byte x2,byte y2,byte gif);															//OLED长方形显示函数
void OLED_Draw_BMP(uint8 x, uint8 y, uint8 row, uint8 column, void *bmp);//OLED画图函数
//void Draw_LQLogo(void);
//void Draw_LibLogo(void);

void OLED_Draw_BMP80x60(void *bmp);
#endif

