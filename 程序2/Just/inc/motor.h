#ifndef __MOTOR_H
#define __MOTOR_H

#include "MK60_ftm.h"
#include "common.h"

extern void motor_init();
extern void motor1_go(uint16 duty);
extern void motor1_back(uint16 duty);
extern void motor2_go(uint16 duty);
extern void motor2_back(uint16 duty);
extern void motor1_stop(void);
extern void motor2_stop(void);

#endif