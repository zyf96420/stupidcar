#ifndef __PID_H
#define __PID_H

void Motor_PID_init(void);
void Steer_PID_init();
int PID_speed(int Actualspeed,int Setspeed);
int PID_speed_left(int Actualspeed,int Setspeed);
int PID_speed_right(int Actualspeed,int Setspeed);
int PID_Adjust(int Setspeed);
int PID_Adjust_Left(int Setspeed);
int PID_Adjust_Right(int Setspeed);
int PID_steer(int NextPoint);

#endif