#ifndef __INFRARED_H
#define __INFRARED_H

#include "include.h"

extern uint16 distance_left;
extern uint16 distance_mid;
extern uint16 distance_right;

void infrared_init(void);
uint8 get_distance(void);


#endif

