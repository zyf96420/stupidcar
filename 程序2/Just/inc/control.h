#ifndef __CONTROL_H
#define __CONTROL_H

#include "include.h"

#define Turn_Max_Num 18

typedef enum
{
    State_Stop = 0,
    State_Searching,            //寻找目标
    State_GotoTarget,           //靠近目标
    State_OffTarget,            //熄灭目标
    State_Escape,               //躲避障碍
    State_Bump,                 //被撞
    
    State_NumMax
}Control_State_e;

extern int nomal_speed;
extern uint8 Current_State;
extern char Turn_Index;
extern signed short datax;//x轴加速度
extern signed short datay;//y轴加速度
extern uint8 Turn_Dir[Turn_Max_Num];
extern uint8 Complete;
extern int32 Turn_Speed;
extern int add_speed;

void camera_filter(uint8 *img);
uint8 control_main(void);
//void photo_extract(uint8 *img);
uint8 obstacle_judge(void);
void steer_control(uint8 angle);              //舵机舵机控制,形参为舵机角度
void car_inner_left_run(void);
void car_inner_right_run(void);
void car_external_left_run(void);
void car_external_right_run(void);
void beacon_strike(void);                                 //信标防撞函数
void car_control(void);
void car_control2(void);                 


#endif

