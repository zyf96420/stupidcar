#ifndef __FUNTION_H
#define __FUNTION_H

#include "encounter.h"
#include "motor.h"

#define MOTOR_PID_P  7.1
#define MOTOR_PID_I  5.8
#define MOTOR_PID_D  0.8

#define CONTROL_OUT_MAX_MOTOR  990
#define CONTROL_OUT_MIN_MOTOR -990

extern signed short PID_TEMP;

typedef enum {Speed = 0, Position = !Speed} PidControlMode;//选择速度模式或位置模式

/*绝对式PID算法，接口参数结构类型*/
typedef struct 
{
 /*PID算法接口变量，用于给用户获取或修改PID算法的特性*/
 float kp;     //比例系数
 float ki;     //积分系数
 float kd;     //微分系数
 float errILim;//误差积分上限
 
 float errNow;//当前的误差
 float ctrOut;//控制量输出
 
 /*PID算法内部变量，其值不能修改*/
 float errOld;
 float errP;
 float errI;
 float errD;
 
}PID_AbsoluteType;


/*增量式PID算法，接口参数结构类型*/
typedef struct 
{
 /*PID算法接口变量，用于给用户获取或修改PID算法的特性*/
 float kp;     //比例系数
 float ki;     //积分系数
 float kd;     //微分系数
 
 float errNow; //当前的误差
 float dCtrOut;//控制增量输出
 float  ctrOut;//控制输出
 
 /*PID算法内部变量，其值不能修改*/
 float errOld1;
 float errOld2;
 
}PID_IncrementType;


extern void get_encoder_ONE(void);
	
extern void get_encoder_TWO(void);	


extern void User_PidSpeedControl1(signed int SpeedTag);

extern void User_PidSpeedControl2(signed int SpeedTag);

extern void User_PidPositionControl(signed int Position,signed int SpeedTag);


void UserMotorSpeedSetOne(signed short control);
void UserMotorSpeedSetTwo(signed short control);

#endif
