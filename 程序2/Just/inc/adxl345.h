#ifndef __ADXL345_H
#define __ADXL345_H

#define filter_times 8

#include "include.h"

void adxl345_init(void);
void adxl345_get_data(signed short *datax,signed short *datay);

#endif