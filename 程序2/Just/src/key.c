//////////////////////////////////////////////
//第十一届飞思卡尔智能车大赛
//江苏科技大学 极速涡流
//队    员：俞强 刘晓玺 
//指导老师：陈迅
//按键

#include "key.h"

uint8 key1_enter_flag_old = 0;
uint8 key2_enter_flag_old = 0;
uint8 key3_enter_flag_old = 0;

void key_Init(void)
{
    disable_irq(PORTB_IRQn);
    
    gpio_init (PTB16, GPI, 1);       //按键下，上拉，下降沿触发中断
    port_init(PTB16, ALT1|PULLUP|IRQ_FALLING );

    gpio_init (PTB17, GPI, 1);       //按键左，上拉，下降沿触发中断
    port_init(PTB17, ALT1|PULLUP|IRQ_FALLING );
    
    gpio_init (PTB18, GPI, 1);       //按键右，上拉，下降沿触发中断
    port_init(PTB18, ALT1|PULLUP|IRQ_FALLING );

    gpio_init (PTB19, GPI, 1);       //按键确认，上拉，下降沿触发中断
    port_init(PTB19, ALT1|PULLUP|IRQ_FALLING );

    gpio_init (PTB21, GPI, 1);       //按键退出，上拉，下降沿触发中断
    port_init(PTB21, ALT1|PULLUP|IRQ_FALLING );
	
    enable_irq(PORTB_IRQn);
	
}

uint8 Get_Key1_State_Enter(void)
{
  uint8 state = 0;
  if((key1_enter_flag_old == 0) && (key1_enter_flag == 1))
  {
    state = 1;
  }
  else if((key1_enter_flag_old == 1) && (key1_enter_flag == 0) && (key1_enter_flag_long == 0))
  {
    state = 2;
  }
  else if((key1_enter_flag_old == 1) && (key1_enter_flag_long == 1))
  {
    state = 3;
  }
  else
  {
    state = 0;
  }
  key1_enter_flag_old = key1_enter_flag;
  return state;
}

uint8 Get_Key2_State_Up(void)
{
  uint8 state = 0;
  if((key2_enter_flag_old == 0) && (key2_enter_flag == 1))
  {
    state = 1;
  }
  else if((key2_enter_flag_old == 1) && (key2_enter_flag == 0))
  {
    state = 2;
  }
  else
  {
    state = 0;
  }
  key2_enter_flag_old = key2_enter_flag;
  return state;
}

uint8 Get_Key3_State_Down(void)
{
  uint8 state = 0;
  if((key3_enter_flag_old == 0) && (key3_enter_flag == 1))
  {
    state = 1;
  }
  else if((key3_enter_flag_old == 1) && (key3_enter_flag == 0))
  {
    state = 2;
  }
  else
  {
    state = 0;
  }
  key3_enter_flag_old = key3_enter_flag;
  return state;
}

void OLED_Key(void)
{
  uint8 state_temp;
  state_temp = Get_Key1_State_Enter();
  if(2 == state_temp)
  {
     Current_Display ++ ;
     if(Current_Display >= oled_State_Num)
       Current_Display = Default_State;
     OLED_Clear();
  }
  else if(3 == state_temp)
  {
    Current_Prameter ++;
    if(Current_Prameter >= 3)
       Current_Prameter = 0;
  }
  
  if(2 == Get_Key2_State_Up())
  {
     OLED_Up_Flag = 1 ;
  }
  else
  {
     OLED_Up_Flag = 0 ;
  }
  
  if(2 == Get_Key3_State_Down())
  {
     OLED_Down_Flag = 1;
  }
  else
  {
     OLED_Down_Flag = 0;
  }
}
