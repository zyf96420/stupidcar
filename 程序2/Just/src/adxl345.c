//////////////////////////////////////////////

#include "adxl345.h"

signed short filter_buffx[filter_times];
signed short filter_buffy[filter_times];
uint8 filter_times_count = 0;

uint8 adxl345_read_reg(uint8 reg);
void adxl345_write_reg(uint8 reg, uint8 Data);

void adxl345_init(void)
{
   i2c_init(I2C0,400*1000);
   adxl345_write_reg(0x2E,0x00);
   adxl345_write_reg(0x31,0x0B);   //正负16g，13位
   adxl345_write_reg(0x2C,0x08);   //速率设定12.5，pdf13页
   adxl345_write_reg(0x2D,0x08);   //电源模式，pdf24页

   adxl345_write_reg(0x1E,0x0f);   //X 偏移量 pdf29页
   adxl345_write_reg(0x1F,0x00);   //Y 偏移量
   adxl345_write_reg(0x20,0x05);   //Z 偏移量 
}

void adxl345_get_data(signed short *datax,signed short *datay)
{
  int temp_bufx = 0;
  int temp_bufy = 0;
  uint8 i = 0;
  
  unsigned char dataxL,dataxH;
  unsigned char datayL,datayH;
  
  dataxL = adxl345_read_reg(0x32);
  dataxH = adxl345_read_reg(0x33);
  datayL = adxl345_read_reg(0x34);
  datayH = adxl345_read_reg(0x35);
  
  if(filter_times_count >= filter_times)
  {
    for(i=0;i<filter_times;i++)
    {
      temp_bufx += filter_buffx[i];
      temp_bufy += filter_buffy[i];
    }
    temp_bufx /= filter_times;
    temp_bufy /= filter_times;
    *datax = (signed short)temp_bufx;
    *datay = (signed short)temp_bufy;
    filter_times_count = 0;
  }
  else
  {
    filter_buffx[filter_times_count] = ((signed short)dataxH << 8)+dataxL;
    filter_buffy[filter_times_count] = ((signed short)datayH << 8)+datayL;
    filter_times_count ++;
  }
}

void adxl345_write_reg(uint8 reg, uint8 Data)
{
    i2c_write_reg(I2C0,0x53,reg,Data);
}

uint8 adxl345_read_reg(uint8 reg)
{
    return i2c_read_reg(I2C0,0x53,reg);
}