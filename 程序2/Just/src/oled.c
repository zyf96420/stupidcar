//////////////////////////////////////////////
//第十一届飞思卡尔智能车大赛
//江苏科技大学 海之精灵
//队    员：俞强 刘晓玺 
//指导老师：陈迅
//Oled界面显示

//接线说明
//GND  电源地
//VCC  接5V或3.3v电源
//D0   接PD15（SCL）
//D1   接PD13（SDA）
//RES  接PD14
//DC   接PD11
//CS   接PD12   

#include <stdio.h>
#include <string.h>
#include "oled.h"
#include "oledfont.h"
//OLED的显存
//存放格式如下
//[0]0 1 2 3 ... 127	
//[1]0 1 2 3 ... 127	
//[2]0 1 2 3 ... 127	
//[3]0 1 2 3 ... 127	
//[4]0 1 2 3 ... 127	
//[5]0 1 2 3 ... 127	
//[6]0 1 2 3 ... 127	
//[7]0 1 2 3 ... 127 


extern uint8_t page;
//函数名 OLED_Init
//参  数 无
//返回值 无
//功  能 OLED初始化
void OLED_Init(void)
{
	gpio_init(PTD11,GPO,1);//输出
    gpio_init(PTD12,GPO,1);//输出
    gpio_init(PTD13,GPO,1);//输出
    gpio_init(PTD14,GPO,1);//输出
    gpio_init(PTD15,GPO,1);
		
	OLED_SCLK_Set();
	OLED_CS_Set();	
	OLED_RST_Clr();
	OLED_Delayms(50);
	OLED_RST_Set();

	OLED_WR_Byte(0xAE,OLED_CMD);//--turn off oled panel
	OLED_WR_Byte(0x00,OLED_CMD);//---set low column address
	OLED_WR_Byte(0x10,OLED_CMD);//---set high column address
	OLED_WR_Byte(0x40,OLED_CMD);//--set start line address  Set Mapping RAM Display Start Line (0x00~0x3F)
	OLED_WR_Byte(0x81,OLED_CMD);//--set contrast control register
	OLED_WR_Byte(0xCF,OLED_CMD); // Set SEG Output Current Brightness
	OLED_WR_Byte(0xA1,OLED_CMD);//--Set SEG/Column Mapping     0xa0左篁??? 0xa1?y3?
	OLED_WR_Byte(0xC8,OLED_CMD);//Set COM/Row Scan Direction   0xc0?????? 0xc8?y3?
	OLED_WR_Byte(0xA6,OLED_CMD);//--set normal display
	OLED_WR_Byte(0xA8,OLED_CMD);//--set multiplex ratio(1 to 64)
	OLED_WR_Byte(0x3f,OLED_CMD);//--1/64 duty
	OLED_WR_Byte(0xD3,OLED_CMD);//-set display offset	Shift Mapping RAM Counter (0x00~0x3F)
	OLED_WR_Byte(0x00,OLED_CMD);//-not offset
	OLED_WR_Byte(0xd5,OLED_CMD);//--set display clock divide ratio/oscillator frequency
	OLED_WR_Byte(0x80,OLED_CMD);//--set divide ratio, Set Clock as 100 Frames/Sec
	OLED_WR_Byte(0xD9,OLED_CMD);//--set pre-charge period
	OLED_WR_Byte(0xF1,OLED_CMD);//Set Pre-Charge as 15 Clocks & Discharge as 1 Clock
	OLED_WR_Byte(0xDA,OLED_CMD);//--set com pins hardware configuration
	OLED_WR_Byte(0x12,OLED_CMD);
	OLED_WR_Byte(0xDB,OLED_CMD);//--set vcomh
	OLED_WR_Byte(0x40,OLED_CMD);//Set VCOM Deselect Level
	OLED_WR_Byte(0x20,OLED_CMD);//-Set Page Addressing Mode (0x00/0x01/0x02)
	OLED_WR_Byte(0x02,OLED_CMD);//
	OLED_WR_Byte(0x8D,OLED_CMD);//--set Charge Pump enable/disable
	OLED_WR_Byte(0x14,OLED_CMD);//--set(0x10) disable
	OLED_WR_Byte(0xA4,OLED_CMD);// Disable Entire Display On (0xa4/0xa5)
	OLED_WR_Byte(0xA6,OLED_CMD);// Disable Inverse Display On (0xa6/a7) 
	OLED_WR_Byte(0xAF,OLED_CMD);//--turn on oled panel
	OLED_WR_Byte(0xAF,OLED_CMD); /*display ON*/ 
	OLED_Clear();  //OLED清显示
	OLED_Set_Pos(0,0);  
}


//函数名 OLED_WR_Byte
//参  数 dat:数据 cmd:命令
//返回值 无
//功  能 OLED写字节
#if OLED_MODE == 1
//向SSD1106写入一个字节。
//dat:要写入的数据/命令
//cmd:数据/命令标志 0,表示命令;1,表示数据;
void OLED_WR_Byte(unsigned char dat,unsigned char cmd)
{
	/*
	DATAOUT(dat);	    
	if(cmd)
		OLED_DC_Set();
	else 
		OLED_DC_Clr();	
	
	OLED_CS_Clr();
	OLED_WR_Clr();	  
	OLED_WR_Set();
	OLED_CS_Set();	  
	OLED_DC_Set();	
	*/
} 	    	    
#else
//向SSD1106写入一个字节。
//dat:要写入的数据/命令
//cmd:数据/命令标志 0,表示命令;1,表示数据;
void OLED_WR_Byte(unsigned char dat,unsigned char cmd)
{			  
	byte i = 8;
	
	if(cmd)
		OLED_DC_Set();
	else 
		OLED_DC_Clr();
	
//  asm("nop"); 	
	OLED_CS_Clr();
	for(i = 0;i < 8;i++)
	{			  
		OLED_SCLK_Clr(); 
//   	asm("nop");
		
		if(dat & 0x80)
			OLED_SDIN_Set();
		else 
			OLED_SDIN_Clr();
		
		OLED_SCLK_Set(); 
//     	asm("nop");
		dat <<= 1;   
	}
	
	OLED_CS_Set();
	OLED_DC_Set();  
} 
#endif


//函数名 OLED_Set_Pos
//参  数 x:x轴坐标 y:y轴坐标
//返回值 无
//功  能 设置OLED光标位置
void OLED_Set_Pos(byte x, byte y)
{ 
	OLED_WR_Byte(0xb0 + y,OLED_CMD);
	OLED_WR_Byte(((x & 0xf0) >> 4) | 0x10,OLED_CMD);
	OLED_WR_Byte((x & 0x0f) | 0x01,OLED_CMD); 
} 

//函数名 OLED_Display_On
//参  数 无
//返回值 无
//功  能 开启OLED显示   
void OLED_Display_On(void)
{
	OLED_WR_Byte(0X8D,OLED_CMD);  //SET DCDC命令
	OLED_WR_Byte(0X14,OLED_CMD);  //DCDC ON
	OLED_WR_Byte(0XAF,OLED_CMD);  //DISPLAY ON
}

//函数名 OLED_Display_Off
//参  数 无
//返回值 无
//功  能 关闭OLED显示   
void OLED_Display_Off(void)
{
	OLED_WR_Byte(0X8D,OLED_CMD);  //SET DCDC命令
	OLED_WR_Byte(0X10,OLED_CMD);  //DCDC OFF
	OLED_WR_Byte(0XAE,OLED_CMD);  //DISPLAY OFF
}	

//函数名 OLED_Clear
//参  数 无
//返回值 无
//功  能 清屏函数,清完屏,整个屏幕是黑色的!和没点亮一样!!!	 
void OLED_Clear(void)  
{  
	unsigned char i,n;	
	
	for(i = 0;i < 8;i++)  
	{  
		OLED_WR_Byte (0xb0 + i,OLED_CMD);    //设置页地址（0~7）
		OLED_WR_Byte (0x00,OLED_CMD);      //设置显示位置—列低地址
		OLED_WR_Byte (0x10,OLED_CMD);      //设置显示位置—列高地址
		for(n = 0;n < 130;n++)
			OLED_WR_Byte(0,OLED_DATA); 
	} //更新显示
}

//在指定位置显示一个字符,包括部分字符
//x:0~127
//y:0~63
//mode:0,反白显示;1,正常显示				 
//size:选择字体 16/12 
void OLED_ShowChar(unsigned char x,unsigned char y,unsigned char chr,unsigned size)
{      	
	unsigned char c = 0,i = 0;	
	
	c = chr - ' ';//得到偏移后的值		
	if(x > Max_Column - 1)
	{
		x = 0;
		y = y + 2;
	}
	
	if(size == 16)
	{
		OLED_Set_Pos(x,y);	
		for(i = 0;i < 8;i++)
			OLED_WR_Byte(F8X16[c * 16 + i],OLED_DATA);
		
		OLED_Set_Pos(x,y + 1);
		for(i = 0;i < 8;i++)
			OLED_WR_Byte(F8X16[c * 16 + i + 8],OLED_DATA);
	}
	else if(size == 8)
	{	
		OLED_Set_Pos(x,y);
		for(i = 0;i<6;i++)
			OLED_WR_Byte(F6x8[c][i],OLED_DATA);			
	}
        else
        {
              OLED_Set_Pos(x,y+1);
		for(i = 0;i<6;i++)
			OLED_WR_Byte(F6x8[c][i],OLED_DATA);	
              
        }
}

//m^n函数
unsigned int oled_pow(unsigned char m,unsigned char n)
{
	unsigned int result = 1;	
	
	while(n--)
		result *= m; 
	
	return result;
}

//显示两位浮点数函数
void OLED_Showfloat(unsigned char x,unsigned char y,double i,unsigned char size)
{
  unsigned int a[3]={0};
  a[0]=(int)i;
  a[1]=(int)((i-(int)i)*10);
  a[2]=(int)((i-a[0])*100-a[1]*10);
  if(a[2]==9)a[1] += 1;
  OLED_ShowNum(x,y,a[0],1,size);
  OLED_ShowChar(x+size/2,y,'.',size);
  OLED_ShowNum(x+size,y,a[1],1,size);
}

//显示2个数字
//x,y :起点坐标	 
//len :数字的位数
//size:字体大小
//mode:模式	0,填充模式;1,叠加模式
//num:数值(0~4294967295);	  		  
void OLED_ShowNum(unsigned char x,unsigned char y,unsigned int num,unsigned char len,unsigned char size)
{         
	unsigned char t,temp;
	unsigned char enshow = 0;	
	
	for(t = 0;t < len;t++)
	{
		temp = (num / oled_pow(10,len - t - 1)) % 10;
		if(enshow == 0 && t < (len - 1))
		{
			if(temp==0)
			{
				OLED_ShowChar(x + (size / 2) * t,y,' ',size);
				continue;
			}
			else 
				enshow = 1; 	 	 
		}
		
		OLED_ShowChar(x + (size / 2) * t,y,temp + '0',size); 
	}
} 

//显示2个数字
//x,y :起点坐标	 
//*chr:显示字符串
void OLED_ShowString(unsigned char x,unsigned char y,unsigned char *chr,unsigned size)
{
        unsigned char i = 8;
	unsigned char j = 0;
        
        if(size == 8)
          i = 6;
	
	while (chr[j] != '\0')
	{
		OLED_ShowChar(x,y,chr[j],size);
		x += i;
		if(x > 120)
		{
		   x = 0;
		   y += 2;
		}
		
		j++;
	}
}

//显示2个数字
//x,y :起点坐标	 
//no  :汉字编号
void OLED_ShowCHinese(unsigned char x,unsigned char y,unsigned char no)
{      			    
	unsigned char t,adder = 0;
	
	OLED_Set_Pos(x,y);	
	for(t = 0;t < 16;t++)
	{
		OLED_WR_Byte(HZK_EZP[2 * no][t],OLED_DATA);
		adder += 1;
	}
	
	OLED_Set_Pos(x,y + 1);	
	for(t = 0;t < 16;t++)
	{	
		OLED_WR_Byte(HZK_EZP[2 * no + 1][t],OLED_DATA);
		adder += 1;
	}					
}

//显示2个数字
//x,y :起点坐标	 
//no  :汉字编号
void OLED_ShowCHinese_EZP(unsigned char x,unsigned char y,unsigned char no)
{      			    
	unsigned char t,adder = 0;
	
	OLED_Set_Pos(x,y);	
	for(t = 0;t < 16;t++)
	{
		OLED_WR_Byte(HZK_EZP[2 * no][t],OLED_DATA);
		adder += 1;
	}	
	
	OLED_Set_Pos(x,y + 1);	
	for(t = 0;t < 16;t++)
	{	
		OLED_WR_Byte(HZK_EZP[2 * no + 1][t],OLED_DATA);
		adder += 1;
	}					
}

//显示2个数字
//x,y :起点坐标	 
//no  :汉字编号
void OLED_ShowCHinese_JISUWOLIU(unsigned char x,unsigned char y,unsigned char no)
{      			    
	unsigned char t,adder = 0;
	
	OLED_Set_Pos(x,y);	
	for(t = 0;t < 16;t++)
	{
		OLED_WR_Byte(JISUWOLIU[2 * no][t],OLED_DATA);
		adder += 1;
	}
	
	OLED_Set_Pos(x,y + 1);	
	for(t = 0;t < 16;t++)
	{	
		OLED_WR_Byte(JISUWOLIU[2 * no + 1][t],OLED_DATA);
		adder += 1;
	}					
}

//函数名 OLED_ShowStart
//参  数 无
//返回值 无
//功  能 极速涡流启动界面
void OLED_ShowStart(void)
{
	OLED_ShowCHinese_EZP(4,1,0);
	OLED_ShowCHinese_EZP(24,1,1);
	OLED_ShowCHinese_EZP(44,1,2);
	OLED_ShowCHinese_EZP(64,1,3);
	OLED_ShowCHinese_EZP(84,1,4);
	OLED_ShowCHinese_EZP(104,1,5);
	
	OLED_ShowCHinese_JISUWOLIU(22,4,0);
	OLED_ShowCHinese_JISUWOLIU(44,4,1);
	OLED_ShowCHinese_JISUWOLIU(66,4,2);
	OLED_ShowCHinese_JISUWOLIU(88,4,3);
	
	page=1;
}

//==============================================================
//函数名: void LCD_Rectangle(byte x1,byte y1,byte x2,byte y2,byte color,byte gif)
//参  数: 起点(x1,y1),终点(x2,y2)
//      x1,x2范围:0~127  y1,y2范围:0~63
//返回值: 无
//功  能: 画长方形
//==============================================================
void LCD_Rectangle(byte x1,byte y1,byte x2,byte y2,byte gif)
{
	byte n; 
				  
	OLED_Set_Pos(x1,y1 >> 3);
	for(n = x1;n <= x2;n++)
	{
		OLED_WR_Byte(0x01 << (y1 % 8),1); 			
		if(gif == 1) 	
			OLED_Delayms(50);
	}  
	
	OLED_Set_Pos(x1,y2 >> 3);
	for(n = x1;n <= x2;n++)
	{
		OLED_WR_Byte(0x01 << (y2 % 8),1); 			
		if(gif == 1) 	
			OLED_Delayms(5);
	}
	
}

/***********功能描述：显示显示BMP图片128×64起始点坐标(x,y),x的范围0～127，y为页的范围0～7，row为行数，column为列数,*bmp图像指针*****************/
void OLED_Draw_BMP(uint8 x, uint8 y, uint8 row, uint8 column, void *bmp)
{
  uint8 i, j;
  uint16 z=0;
  uint8 index = 0;
  uint8 logic[8] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};
  uint8 temp = 0;
  uint8_t * tt = (uint8_t *)bmp;


  if(row%8 == 0)
    row = row/8; 
  else
    row = row/8 + 1;
  
  for(i=y; i< y+row; i++)
  {
    OLED_Set_Pos(x, i);
    for(j=x; j< x+column; j++)
    { 
      if(tt[z] != 0xFF)
      
      temp = (tt[z] & logic[index]) >> index;
      if(temp == 1)
      {
        OLED_WR_Byte(0xFF,OLED_DATA) ;
      }
      else if(temp == 0)
        OLED_WR_Byte(0x00,OLED_DATA) ;
      else
      {
      }
      
      if(index >= 7)
      {
        index = 0;
        z++;
      }
      else 
        index++;
    }      
  }
}

void OLED_Draw_BMP80x60(void *bmp)
{
  uint8 i = 0;
  bool temp;
  uint8 page = 0;
  uint8 col_page = 0;
  uint8 col_num = 0;
  uint8 mask[8] = {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};
  uint8 col = 10;
  uint8 result = 0;
  uint8_t *temp_bmp = (uint8_t *)bmp;
  
  
  for(page = 1;page < 9;page++)
  {
    OLED_Set_Pos(0,page-1);
    for(col_page = 0;col_page < col;col_page++)
    {
      for(col_num = 0;col_num < 8;col_num++)
      {
        for(i = 0;i < 8;i++)
        {
          result <<= 1;
          temp = (*(temp_bmp + ((70*page - i*10) + col_page)) & mask[col_num]);
          result |= temp;
        }
        OLED_WR_Byte(result,OLED_DATA) ;
      }
    }
  }
}

//函数名 OLED_Delayms
//参  数 ms:延时毫秒数
//返回值 无
//功  能 OLED延时
void OLED_Delayms(unsigned int ms)
{                         
	word a;
	
	while(ms)
	{
		a = 13350;
		while(a--);
		
		ms--;
	}
	
	return;
}
/*
void Draw_LQLogo(void)
{ 	
	word ii = 0;
	byte x,y;       

	for(y = 0;y < 8;y++)
	{
		LCD_Set_Pos(16,y);				
		for(x = 16;x < 112;x++)
		{      
			LCD_WrDat(longqiu96x64[ii++]);	    	
		}
	}
}

void Draw_LibLogo(void)
{ 	
	word ii = 0;
	byte x,y;  

	for(y = 0;y < 8;y++)
	{
		LCD_Set_Pos(34,y);				
		for(x = 34;x < 94;x++)
		{      
			LCD_WrDat(LIBLOGO60x58[ii++]);	    	
		}
	}
} 
*/


