//////////////////////////////////////////////
//第十一届飞思卡尔智能车大赛
//江苏科技大学 极速涡流
//队    员：俞强 刘晓玺 
//指导老师：陈迅
//中断服务函数

#include "ISR.h"
#include "UserFunction.h"

int Spd = 0;   //电机期望速度

uint8 PTB18_enter_times = 0;
uint8 key1_enter_flag = 0;
uint8 key1_enter_flag_long = 0;

uint8 PTB16_enter_times = 0;
uint8 key2_enter_flag = 0;

uint8 PTB17_enter_times = 0;
uint8 key3_enter_flag = 0;

char Start_Count_Flag = 0;
unsigned short Time = 0;
char display_data[16];

int32 tick_10ms = 0;

uint16 duzhuan_count = 0;
/*!
 *  @brief      PORTA中断服务函数
 *  @since      v5.0
 */
void PORTA_IRQHandler()
{
    uint8  n;    //引脚号
    uint32 flag;

    while(!PORTA_ISFR);
    flag = PORTA_ISFR;
    PORTA_ISFR  = ~0;                                   //清中断标志位

    n = 29;                                             //场中断
    if(flag & (1 << n))                                 //PTA29触发中断
    {
        camera_vsync();
    }
#if ( CAMERA_USE_HREF == 1 )                            //使用行中断
    n = 28;
    if(flag & (1 << n))                                 //PTA28触发中断
    {
        camera_href();
    }
#endif
//  OLED_Draw_BMP(0,0,60,80,imgbuff);

}

/*!
 *  @brief      DMA0中断服务函数
 *  @since      v5.0
 */
void DMA0_IRQHandler()
{
    camera_dma();
}

///***********************************
//       按键中断服务函数
//***********************************/
void PORTB_IRQHandler()
{
  disable_irq(PORTB_IRQn);
  if(PORTB_ISFR & (1<<19))                                //判断是不是按下了key_enter键
  {

      
  }
   PORTB_ISFR  = ~0; //清中断标志位
   enable_irq(PORTB_IRQn);
}

void lptmr_hander(void)
{
   LPTMR0_CSR |= LPTMR_CSR_TCF_MASK;         //清除LPT比较标志位
   tick_10ms++;

   if(!gpio_get(PTB18))
   {
       PTB18_enter_times += 1;
       if(PTB18_enter_times >= 2)           //连续检测到两次为低电平就肯定按下了
       {
         key1_enter_flag = 1;
       }
       else
       {
         key1_enter_flag = 0;
       }
       if(PTB18_enter_times >= 100)
       {
         PTB18_enter_times = 100;
         key1_enter_flag_long = 1;
         key1_enter_flag = 0;
       }
       else
       {
         key1_enter_flag_long = 0;
       }
   }
   else
   {
     PTB18_enter_times = 0;
     key1_enter_flag_long = 0;
     key1_enter_flag = 0;
   }
   
   if(!gpio_get(PTB16))
   {
       PTB16_enter_times += 1;
       if(PTB16_enter_times >= 2)           //连续检测到两次为低电平就肯定按下了
       {
         key2_enter_flag = 1;
       }
       else
       {
         key2_enter_flag = 0;
       }
       if(PTB16_enter_times >= 100)
       {
         PTB16_enter_times = 100;
       }
   }
   else
   {
     PTB16_enter_times = 0;
     key2_enter_flag = 0;
   }
   
   if(!gpio_get(PTB17))
   {
       PTB17_enter_times += 1;
       if(PTB17_enter_times >= 2)           //连续检测到两次为低电平就肯定按下了
       {
         key3_enter_flag = 1;
       }
       else
       {
         key3_enter_flag = 0;
       }
       if(PTB17_enter_times >= 100)
       {
         PTB17_enter_times = 100;
       }
   }
   else
   {
     PTB17_enter_times = 0;
     key3_enter_flag = 0;
   }
   
   if(Start_Count_Flag)
   {
     if(Complete)
     {
       Time /= 10;
       sprintf(display_data,"%d.%d s",Time/10,Time%10);
#ifdef BLU
      uart_putstr(UART5,(uint8 *)display_data);
      uart_putchar(UART5,1);
      uart_putchar(UART5,120);
#endif       
      Start_Count_Flag = 0;
     }
     else
     {
       Time ++;
     }
   }
   if(Current_State == State_Searching || Current_State == State_GotoTarget)
   {
     if(Motor_Spd_Now < 25)           //速度检测，如果速度一直很小说明堵转
     {
       duzhuan_count ++;///////////////////////////////////////////////////
     }
     else
     {
       duzhuan_count = 0;
     }
   }
   else
   {
     duzhuan_count = 0;
   }
}

void PORTE_IRQHandler()
{
    uint8  n;    //引脚号
    uint32 flag;

    flag = PORTE_ISFR;
    PORTE_ISFR  = ~0;                                   //清中断标志位

    n = 27;
    if(flag & (1 << n))                                 //PTE27触发中断
    {
        nrf_handler();
    }
}

#ifdef BLU
void UART5_IRQHandler()
{
  UARTn_e uratn = UART5;
  char aa;

  if(UART_S1_REG(UARTN[uratn]) & UART_S1_RDRF_MASK)   //接收数据寄存器满
  {
       uart_getchar(UART5,&aa);
       if(0x30 == aa)
       {
         Complete = 1;
         Current_State = State_Stop;
//#ifdef BLU
//         uart_putstr(UART5,"STOP\r\n");
//#endif
       }
  }
}
#endif

void PIT2_IRQHandler()
{
   encounter_data();                                   //读取编码器的值
   User_PidSpeedControl1(Spd);
   PIT_Flag_Clear(PIT2);                                   //清中断标志位
}
