//////////////////////////////////////////////
//第十一届飞思卡尔智能车大赛
//江苏科技大学 极速涡流
//队    员：俞强 刘晓玺 
//指导老师：陈迅
//舵机驱动

#include "steer.h"

extern double p_steer1;
extern double i_steer1;
extern double d_steer1;

extern double p_steer2;
extern double i_steer2;
extern double d_steer2;

extern uint16 beacon_W;

int degree_adjust = 0;		            //舵机打角调整量

void steer_init()
{
  ftm_pwm_init(FTM3, steer_channel, 100, 0);   //PTD0
  steer_reset();
}

void steer_turn(int8 degree)            //朝左负，朝右正
{
  uint8 duty;
  uint8 steer_duty;
  if(degree >= 90)degree = 90;
  if(degree <= -90)degree = -90;
	
  if(degree > 0)
  {
     duty = (uint8)(0.4 * (-degree));
     steer_duty = 150 - (int)duty;
  }
  else 
  {
     duty = (uint8)(0.4 *(degree));
     steer_duty = 150  + (int)duty;
  }
  ftm_pwm_duty(FTM3, steer_channel, steer_duty);
}

void steer_reset()
{
  steer_turn(0);
}

void steer_pid_turn()
{ 
  degree_adjust = PID_steer(beacon_W);
  
  if(degree_adjust > 45)degree_adjust = 45;
  if(degree_adjust < -45)degree_adjust = -45;
  
  steer_turn(90-degree_adjust);
}


