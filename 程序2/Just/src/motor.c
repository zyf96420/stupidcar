//////////////////////////////////////////////
//第十一届飞思卡尔智能车大赛
//江苏科技大学 极速涡流
//队    员：俞强 刘晓玺 
//指导老师：陈迅
//电机驱动

#include "motor.h"


//电机初始化函数,频率为10KHz
void motor_init()
{
  ftm_pwm_init(FTM0,FTM_CH0,20000,0);//PTC1
  ftm_pwm_init(FTM0,FTM_CH1,20000,0);//PTC2
  ftm_pwm_init(FTM0,FTM_CH2,20000,0);//PTC3
  ftm_pwm_init(FTM0,FTM_CH3,20000,0);//PTC4
}

void motor1_go(uint16 duty)
{
  ftm_pwm_duty(FTM0, FTM_CH0,duty);      //左电机正转 
  ftm_pwm_duty(FTM0, FTM_CH1,0);
}

void motor1_back(uint16 duty)
{
  ftm_pwm_duty(FTM0, FTM_CH1,duty);      //左电机反转 
  ftm_pwm_duty(FTM0, FTM_CH0,0);
}

void motor2_go(uint16 duty)
{
  ftm_pwm_duty(FTM0, FTM_CH3,duty);       //右电机正转 
  ftm_pwm_duty(FTM0, FTM_CH2,0);
}

void motor2_back(uint16 duty)
{
  ftm_pwm_duty(FTM0, FTM_CH2,duty);        //右电机反转 
  ftm_pwm_duty(FTM0, FTM_CH3,0);
}

void motor1_stop()                         //左电机停转    
{
  ftm_pwm_duty(FTM0, FTM_CH0,0);
  ftm_pwm_duty(FTM0, FTM_CH1,0);
}

void motor2_stop()                         //右电机停转
{
  ftm_pwm_duty(FTM0, FTM_CH2,0);
  ftm_pwm_duty(FTM0, FTM_CH3,0);
}


