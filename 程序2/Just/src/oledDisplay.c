#include "oledDisplay.h"

unsigned char Current_Display = 0;
unsigned char Current_Prameter = 0;
unsigned char OLED_Up_Flag = 0;
unsigned char OLED_Down_Flag = 0;
unsigned char Direct_Count = 0;

char oled_tempbuf_short[8];
char oled_tempbuf_long[16];

void oled_Display_Default(void);
void oled_Display_Camera(void);
void oled_Display_Prameter(void);
void oled_Display_State(void);
void oled_Direct_Mode(void);

void oled_Display(void)
{
  OLED_Key();
  switch(Current_Display)
    {
    case Default_State:
      {
        oled_Display_Default();
        break;
      }
    case Camera_State:
      {
        oled_Display_Camera();
        break;
      }
     case State_State:
      {
        oled_Display_State();
        break;
      }
    case Prameter_State:
      {
        oled_Display_Prameter();
        break;
      }
    case Direct_State:
      {
        oled_Direct_Mode();
        break;
      }
    default:break;
    }
}

void oled_Display_Default(void)
{
    OLED_ShowString(0,0,(unsigned char*)"             ",8);//显示增加速度
    OLED_ShowString(0,0,(unsigned char*)"AS:",8);
    sprintf(oled_tempbuf_short,"%d",add_speed);
    OLED_ShowString(27,0,(unsigned char*)oled_tempbuf_short,8);
    
    if(OLED_Up_Flag)
    {
      add_speed += 5;
      OLED_ShowString(0,0,(unsigned char*)"             ",8);
    }
    if(OLED_Down_Flag)
    {
      add_speed -= 5;
      OLED_ShowString(0,0,(unsigned char*)"             ",8);
    }
}

void oled_Display_Camera(void)
{
    OLED_Draw_BMP80x60(imgbuff);//显示摄像头采集的图片
    
    OLED_ShowString(81,0,(unsigned char*)"       ",8);//显示速度
    sprintf(oled_tempbuf_short,"%d",Motor_Spd_Now);
    OLED_ShowString(81,0,(unsigned char*)oled_tempbuf_short,8);
    
//    OLED_ShowString(81,1,(unsigned char*)"X:",3);//显示目标大致X坐标
//    OLED_ShowString(81,2,(unsigned char*)"   ",3);
//    sprintf(oled_tempbuf_short,"%d",Target_X);
//    OLED_ShowString(81,2,(unsigned char*)oled_tempbuf_short,4);
    
    OLED_ShowString(81,3,(unsigned char*)"Y:",3);//显示目标大致Y坐标
    OLED_ShowString(81,4,(unsigned char*)"   ",3);
    sprintf(oled_tempbuf_short,"%d",Target_Y);
    OLED_ShowString(81,4,(unsigned char*)oled_tempbuf_short,4);
    
    switch(Current_State)
    {
    case State_Stop:
      OLED_ShowString(81,5,(unsigned char*)"   ",4);
      OLED_ShowString(81,5,(unsigned char*)"OFF",4);
      break;
    case State_Searching:
      OLED_ShowString(81,5,(unsigned char*)"   ",4);
      OLED_ShowString(81,5,(unsigned char*)"SER",4);
      break;
    case State_GotoTarget:
      OLED_ShowString(81,5,(unsigned char*)"   ",4);
      OLED_ShowString(81,5,(unsigned char*)"GOO",4);
      break;
    case State_OffTarget:
      OLED_ShowString(81,5,(unsigned char*)"   ",4);
      OLED_ShowString(81,5,(unsigned char*)"OFT",4);
      break;
    case State_Escape:
      OLED_ShowString(81,5,(unsigned char*)"   ",4);
      OLED_ShowString(81,5,(unsigned char*)"ESC",4);
      break;
    case State_Bump:
      OLED_ShowString(81,5,(unsigned char*)"   ",4);
      OLED_ShowString(81,5,(unsigned char*)"BMP",4);
      break;
    default:break;
    }
    
    OLED_ShowString(81,1,(unsigned char*)"W:",3);
    OLED_ShowString(81,2,(unsigned char*)"    ",5);
    sprintf(oled_tempbuf_short,"%d",Target_Width);
    OLED_ShowString(81,2,(unsigned char*)oled_tempbuf_short,5);
    
    OLED_ShowString(81,6,(unsigned char*)"    ",5);
    sprintf(oled_tempbuf_short,"%d",Turn_Index);
    OLED_ShowString(81,6,(unsigned char*)oled_tempbuf_short,5);
}

void oled_Display_State(void)
{  
    OLED_ShowString(0,0,(unsigned char*)"          ",8);//显示
    OLED_ShowString(0,0,(unsigned char*)"AX:",8);
    sprintf(oled_tempbuf_short,"%d",datax);
    OLED_ShowString(27,0,(unsigned char*)oled_tempbuf_short,8);
    
    OLED_ShowString(0,1,(unsigned char*)"          ",8);//显示
    OLED_ShowString(0,1,(unsigned char*)"AY:",8);
    sprintf(oled_tempbuf_short,"%d",datay);
    OLED_ShowString(27,1,(unsigned char*)oled_tempbuf_short,8);
    
    OLED_ShowString(0,2,(unsigned char*)"          ",8);//显示
    OLED_ShowString(0,2,(unsigned char*)"PO:",8);
    sprintf(oled_tempbuf_short,"%d",PID_TEMP);
    OLED_ShowString(27,2,(unsigned char*)oled_tempbuf_short,8);
    
    OLED_ShowString(0,3,(unsigned char*)"          ",8);//显示
    OLED_ShowString(0,3,(unsigned char*)"TS:",8);
    sprintf(oled_tempbuf_short,"%d",Turn_Speed);
    OLED_ShowString(27,3,(unsigned char*)oled_tempbuf_short,8);
}

void oled_Display_Prameter(void)
{
    OLED_ShowString(0,0,(unsigned char*)"             ",8);//显示标准速度
    OLED_ShowString(0,0,(unsigned char*)"NS:",8);
    sprintf(oled_tempbuf_short,"%d",nomal_speed);
    OLED_ShowString(27,0,(unsigned char*)oled_tempbuf_short,8);
    
    switch (Current_Prameter)
    {
    case 0:
       OLED_ShowString(60,0,(unsigned char*)"o",8);
       OLED_ShowString(60,1,(unsigned char*)" ",8);
       OLED_ShowString(60,2,(unsigned char*)" ",8);
       break;
    case 1:
       OLED_ShowString(60,0,(unsigned char*)" ",8);
       OLED_ShowString(60,1,(unsigned char*)"o",8);
       OLED_ShowString(60,2,(unsigned char*)" ",8);
       break;
    case 2:
       OLED_ShowString(60,0,(unsigned char*)" ",8);
       OLED_ShowString(60,1,(unsigned char*)" ",8);
       OLED_ShowString(60,2,(unsigned char*)"o",8);
       break;
    default:break;
    }
    
    if(OLED_Up_Flag)
    {
      nomal_speed += 5;
      OLED_ShowString(0,0,(unsigned char*)"             ",8);
    }
    if(OLED_Down_Flag)
    {
      nomal_speed -= 5;
      OLED_ShowString(0,0,(unsigned char*)"             ",8);
    }
}

void oled_Direct_Mode(void)
{
  unsigned char i = 0;
  for(i=0;i<Turn_Max_Num;i++)
  {
    if(i < 15)
      OLED_ShowChar(8*i,0,Turn_Dir[i]+0x30,8);
    else
      OLED_ShowChar(8*(i-15),2,Turn_Dir[i]+0x30,8);
  }
  
  if(Direct_Count < 15)
    OLED_ShowChar(8*Direct_Count,1,'o',8);
  else
    OLED_ShowChar(8*(Direct_Count-15),3,'o',8);
  
  if(OLED_Up_Flag)
  {
    Direct_Count++;
    if(Direct_Count >= Turn_Max_Num)
      Direct_Count = 0;
    OLED_ShowString(0,1,(unsigned char*)"                    ",8);
    OLED_ShowString(0,3,(unsigned char*)"                    ",8);
  }
  
  if(OLED_Down_Flag)
  {
    Turn_Dir[Direct_Count]++;
    if(Turn_Dir[Direct_Count] >= 2)
      Turn_Dir[Direct_Count] = 0;
  }
}