//////////////////////////////////////////////
//第十一届飞思卡尔智能车大赛
//江苏科技大学 极速涡流
//队    员：俞强 刘晓玺 
//指导老师：陈迅
//拨码开关函数


#include "boma.h"

void boma_init()
{
  gpio_init(KBM1, GPI, 1);         //拨码开关1
  gpio_init(KBM2, GPI, 1);         //拨码开关2
  gpio_init(KBM3, GPI, 1);         //拨码开关3
  gpio_init(KBM4, GPI, 1);         //拨码开关4
}