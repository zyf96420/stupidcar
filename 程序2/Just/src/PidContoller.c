#include "UserFunction.h"

signed short PID_TEMP;

void PID_AbsoluteMode(PID_AbsoluteType* PID)	//绝对式PID算法
{
 if(PID->kp      < 0)    PID->kp      = -PID->kp;
 if(PID->ki      < 0)    PID->ki      = -PID->ki;
 if(PID->kd      < 0)    PID->kd      = -PID->kd;
 if(PID->errILim < 0)    PID->errILim = -PID->errILim;

 PID->errP = PID->errNow;  //读取现在的误差，用于kp控制

 PID->errI += PID->errNow; //误差积分，用于ki控制

 if(PID->errILim != 0)	   //积分上限和下限
 {
  if(     PID->errI >  PID->errILim)    PID->errI =  PID->errILim;
  else if(PID->errI < -PID->errILim)    PID->errI = -PID->errILim;
 }
 
 PID->errD = PID->errNow - PID->errOld;//误差微分，用于kd控制

 PID->errOld = PID->errNow;	//保存现在的误差
 
 PID->ctrOut = PID->kp * PID->errP + PID->ki * PID->errI + PID->kd * PID->errD;//计算绝对式PID输出

}

void PID_IncrementMode(PID_IncrementType* PID)//增量式PID算法
{
 float dErrP, dErrI, dErrD;
 
 if(PID->kp < 0)    PID->kp = -PID->kp;
 if(PID->ki < 0)    PID->ki = -PID->ki;
 if(PID->kd < 0)    PID->kd = -PID->kd;

 dErrP = PID->errNow - PID->errOld1;

 dErrI = PID->errNow;

 dErrD = PID->errNow - 2 * PID->errOld1 + PID->errOld2;

 PID->errOld2 = PID->errOld1; //二阶误差微分
 PID->errOld1 = PID->errNow;  //一阶误差微分

 /*增量式PID计算*/
 PID->dCtrOut = PID->kp * dErrP + PID->ki * dErrI + PID->kd * dErrD;
 
 if(PID->kp == 0 && PID->ki == 0 && PID->kd == 0)   PID->ctrOut = 0;

 else PID->ctrOut += PID->dCtrOut;
 
 if(PID->ctrOut > CONTROL_OUT_MAX_MOTOR)			//防溢出
	 PID->ctrOut = CONTROL_OUT_MAX_MOTOR;
 if(PID->ctrOut < CONTROL_OUT_MIN_MOTOR)			//防溢出
	 PID->ctrOut = CONTROL_OUT_MIN_MOTOR;
}


/*****************************************电机速度环伺服***********************************************/
int spdTag, spdNow, control;			        //定义一个目标速度，采样速度，控制量

PID_IncrementType PID_Control5;				//定义增量式PID结构体变量
PID_IncrementType PID_Control6;


void User_PidSpeedControl1(int SpeedTag)//底盘电机1PID
{
  spdNow = Motor_Spd_Now; spdTag = SpeedTag;
	
  PID_Control5.errNow = spdTag - spdNow;
	
  PID_Control5.kp			= MOTOR_PID_P;
  PID_Control5.ki			= MOTOR_PID_I;
  PID_Control5.kd			= MOTOR_PID_D;

  PID_IncrementMode(&PID_Control5);

  control = (int)PID_Control5.ctrOut;
  
  PID_TEMP = control;
	
  UserMotorSpeedSetOne(control);
}


void User_PidSpeedControl2(int SpeedTag)//底盘电机2PID
{
//  spdNow = MOTOR.wheel_speed[1]; spdTag = SpeedTag;
	
//	PID_Control6.errNow = spdTag - spdNow;
	
//	PID_Control6.kp			= MOTOR_PID_P;
//	PID_Control6.ki			= MOTOR_PID_I;
//	PID_Control6.kd			= MOTOR_PID_D;
//	PID_IncrementMode(&PID_Control6);
	
//	control = PID_Control6.ctrOut;
	
//	UserMotorSpeedSetTwo(control);
}


/**************************************************************************************/
void UserMotorSpeedSetOne(signed short control)//电机1转速/转向设置
{
	if(control >= 0)
        {
          motor1_go(control);
        }
        else
        {
          motor1_back(control * -1);
        }
}

void UserMotorSpeedSetTwo(signed short control)//电机2转速/转向设置
{		
//	CAN.tx_dat[3] = control;
//	CAN.tx_dat[2] = control >> 8;
}



