//////////////////////////////////////////////
//第十二届飞思卡尔智能车大赛
//1

#include "control.h"
#include "UserFunction.h"
#include "include.h"

signed short datax;//x轴加速度
signed short datay;//y轴加速度

uint8 Current_State = 0;                        //小车当前状态
uint8 State_Before_Esc = 0;                    //小车避障前状态
uint8 Last_State = 0;                          //小车前次状态
uint8 Target_Found_Flag = 0;                    //找到目标标志位

uint8 Turn_Dir[Turn_Max_Num] = {1,1,0,1,1,1,0,1,1,1,1,0,1,0,0,0,0};//0朝右，1朝左
//uint8 Turn_Dir[Turn_Max_Num] = {1,1,1,1,0,0,0,0,0,1,1,0,0,0};//0朝右，1朝左
char Turn_Index = 0;
uint8 Next_Search_Turn = 0; //下一次搜索的转向，1为向左，0为向右
uint8 Searching_Delay = 0;//当没找到目标时，延时一次再转为搜索模式
uint8 Complete = 0;         //任务完成标志位
uint8 Turn_Flag = 0;        //准备切换到下一个信标
int8 Steer_Table_Left0[10] = {-50,-40,-40,-30,-30,-20,-15, 0, 20,20}; //下次向左转(目标处于较近距离)
int8 Steer_Table_Right0[10]= {-20,-10, 0,10,20, 25, 25, 40, 40,50}; //下次向右转(目标处于较近距离)
//int8 Steer_Table_Left1[10] = {-30,-30,-25,-20,-10,0, 10,25,25,35}; //下次向左转(目标处于较远距离)
//int8 Steer_Table_Right1[10]= {-30,-20,-15,-10 , 0, 15,20,25,30,30}; //下次向右转(目标处于较远距离)

int8 Steer_Table_Left1[10] = {-35,-30,-25,-15, -5, 5,15,25,30,45}; //下次向左转(目标处于较远距离)
int8 Steer_Table_Right1[10]= {-45,-25,-23,-13 ,-5, 5,15,25,30,30}; //下次向右转(目标处于较远距离)

int8 Steer_Table_Left2[10] = {-55,-55,-55,-40,-40,-30,-20,-10,0,15}; //下次向左转(熄灭目标)
int8 Steer_Table_Right2[10]= {-15,0,  10, 30, 20, 40, 40, 55, 55,55}; //下次向右转(熄灭目标)
int8 Steer_Table[10]= {0};
int8 Steer_Current = 0;//当前舵机角度（不包括搜索目标时的角度）
int8 Steer_Old = 0;
int32 Turn_Speed;
int32 Turn_Speed_Old;
uint32 Turn_Speed_Keep_Time;

char Distance_Y = 0;
uint8 Target_Y_Old = 0;

int nomal_speed = 95;
int add_speed = 40;
char change_flag = 0;//目标移除屏幕调整标志位
char tempi;
#ifdef BLU
//int32 blu_send_cnt=0;
#endif

void Follow_Target(void);

uint8 control_main(void)    
{
  camera_get_img();                                   //摄像头获取图像，每次采样时间为6.5ms    
//  get_distance();                                     //距离 0 - 3300  小于1200就有可能有东西      
//  adxl345_get_data(&datax,&datay);
  if(Image_Search_Target(imgbuff,&Target_X,&Target_Y,&Target_Width,&Target_Height))  //Target_X 0-9    Target_Y 0 - 59  Target_Width 0-100，Target_Height 0-2550
  {
    
     Target_X = 255;
     Target_Y = 255;
     Target_Width = 0;
     Target_Height = 0;
     Target_Found_Flag = 0;
  }
  else
  {
      Target_Found_Flag = 1;
      if(Target_Width == 0 && Target_Height == 0)
        Target_Found_Flag = 0;
  }
  
   Get_Car_Turn_Speed(&Target_X,&Turn_Speed);
   if(Turn_Speed == Turn_Speed_Old)
   {
     Turn_Speed_Keep_Time ++;
     if(Turn_Speed_Keep_Time > 25)
     {
       Turn_Speed_Keep_Time = 0;
       Turn_Speed = 0;
     }
   }
   else
   {
     Turn_Speed_Keep_Time = 0;
   }
  
 /*******************************************************************************/   
  switch(Current_State)//小车状态机
  {
  case State_Stop:
    {
      Spd = 0;
      steer_turn(0);
      if(Complete == 0)
      {
        if(Target_Found_Flag)
        {
          Current_State = State_Searching;
          Start_Count_Flag = 1;
//#ifdef BLU
//          uart_putstr(UART5,"SEARCH\r\n");
//#endif
        }
      }
      break;
    }
    
 /*********************寻找信标*************************************************/     
   case State_Searching:
   {
     Spd = nomal_speed - 30;
     if(!Turn_Dir[Turn_Index])
      steer_turn(90);
     else if(1 == Turn_Dir[Turn_Index])
      steer_turn(-90);
     Searching_Delay = 0;

     if(Target_Found_Flag)
     {
       if(Target_Y < 40 && Target_Width > 3)//若目标非常近或非常小则不认为是下一个目标
       {
          Current_State = State_GotoTarget;
//#ifdef BLU
//          uart_putstr(UART5,"GO\r\n");
//#endif
       }
     }
     
     if(duzhuan_count > 75)
     {
        State_Before_Esc = Current_State;
        Current_State = State_Escape;
     }
     break;
   }
    
 /*****************避障****************************************************/   
   case State_Escape:                   
   { 
        Spd = -50;
	steer_turn(0);
        DELAY_MS(800);
        duzhuan_count = 0;
        Current_State = State_Before_Esc;
     break;
   }
   
   
 /****************接近信标****************************************************/  
   case State_GotoTarget:
   {
     Searching_Delay = 0;
     if(!Target_Found_Flag)
     {
       if(change_flag)
       {
         steer_turn(Steer_Current);
          if(duzhuan_count > 75)
          {
             State_Before_Esc = Current_State;
             Current_State = State_Escape;
          }
       }
       else
       {
         if(Target_Y_Last_Last < 37)
         {
           if(0 == Target_X_Last_Last || 9 == Target_X_Last_Last)
           {
             steer_turn(Steer_Current);
             change_flag = 1;
           }
         }
         else
         {
            Current_State = State_Searching;
            change_flag = 0;
//#ifdef BLU
//            uart_putstr(UART5,"SEARCH\r\n");
//#endif
         }
       }
     }
     else
     {
       change_flag = 0;
       if(Turn_Flag)
       {
         Turn_Flag = 0;
         Turn_Index ++ ;
//#ifdef BLU
//       uart_putchar(UART5,Turn_Index+'0');
//#endif
         if(Turn_Index >= Turn_Max_Num)
         {
           Turn_Index = 0;
           Complete = 0;
//#ifdef BLU
//           uart_putstr(UART5,"STOP\r\n");
//#endif
         }
       }
       
    if(duzhuan_count > 75)
    {
       State_Before_Esc = Current_State;
       Current_State = State_Escape;
    }
      
     Follow_Target();
     }
     break;
   }
 
 /***************熄灭信标*************************************************/
   case State_OffTarget:
   {
      if(0 == Turn_Dir[Turn_Index])
      {
        for(tempi=0;tempi<10;tempi++)
        {
          Steer_Table[tempi] = (Steer_Table_Right1[tempi]+Target_Y-5);
        }
        steer_turn(Steer_Table[Target_X]);
        Spd = nomal_speed - 35;
      }
      else if(1 == Turn_Dir[Turn_Index])
      {
        for(tempi=0;tempi<10;tempi++)
        {
          Steer_Table[tempi] = (Steer_Table_Left1[tempi]-Target_Y+5);
        }
        steer_turn(Steer_Table[Target_X]);
        Spd = nomal_speed - 35;
      }
      
      if(!Target_Found_Flag || Target_Y > 45)
      {
        if(Searching_Delay)
        {
         Current_State = State_Searching;
//#ifdef BLU
//         uart_putstr(UART5,"SEARCH\r\n");
//#endif
        }
        else
        {
          Searching_Delay ++;
          break;
        }
      }
      
      Get_Target_Offset_Y(Target_Y_Old,Target_Y,&Distance_Y); 
      Target_Y_Old = Target_Y;
      
      if(Distance_Y > 14)//目标突然上移了
      {
        Current_State = State_GotoTarget;
//#ifdef BLU
//        uart_putstr(UART5,"GO\r\n");
//#endif
      }
      break;
   }
   
/***************************************************************************/
   default:break;
  }
  Last_State = Current_State;
  Target_X_Last_Last = Target_X_Last;
  Target_Y_Last_Last = Target_Y_Last;
  Target_X_Last = Target_X;
  Target_Y_Last = Target_Y;
  Target_Width_Last = Target_Width;
  Target_Height_Last = Target_Height;
  Steer_Old = Steer_Current;
  Turn_Speed_Old = Turn_Speed;
#ifdef BLU
    uart_putchar(UART5,Target_X);
    uart_putchar(UART5,Target_Y);
    uart_putchar(UART5,(uint8)Target_Width);
    uart_putchar(UART5,Current_State);
  
    uart_putchar(UART5,0);
    uart_putchar(UART5,120);
#endif  
   return  0;
}

void Follow_Target(void)
{
  if(Target_Width > 7 && Target_Y > 42)//当目标很近///////////////////////////
  {
    Current_State = State_OffTarget;
    Turn_Flag = 1;
#ifdef BLU
      uart_putstr(UART5,"OFF\r\n");
#endif
  }
  else if(Target_Width > 10 && Target_Y > 25 && Target_Y < 100)//当目标较近/////////////////////
   {
     Spd = 85;
     if(0 == Turn_Dir[Turn_Index])//下次向右转
     {
       for(tempi=0;tempi<10;tempi++)
      {
        Steer_Table[tempi] = (Steer_Table_Right1[tempi]+Target_Y-7);
      }
      Steer_Current = Steer_Table[Target_X];
       steer_turn(Steer_Current);
     }
     else if(1 == Turn_Dir[Turn_Index])                    //下次向左转
     {
       for(tempi=0;tempi<10;tempi++)
      {
        Steer_Table[tempi] = (Steer_Table_Left1[tempi]-Target_Y+7);
      }
      Steer_Current = Steer_Table[Target_X];
      steer_turn(Steer_Current);
     }
   } 
  ////////////////当目标较远////////////////////////////////////////////////////
  else if(Target_Y < 100)
  {
    if(0 == Turn_Dir[Turn_Index])//下次向右转
    {
      for(tempi=0;tempi<10;tempi++)
      {
        Steer_Table[tempi] = (Steer_Table_Right1[tempi]+Target_Y - 10);
      }
      Steer_Current = Steer_Table[Target_X];

        steer_turn(Steer_Current);
    }
    else if(1 == Turn_Dir[Turn_Index])                //下次向左转
    {
      for(tempi=0;tempi<10;tempi++)
      {
        Steer_Table[tempi] = (Steer_Table_Left1[tempi]-Target_Y + 10);
      }
      Steer_Current = Steer_Table[Target_X];

        steer_turn(Steer_Current);
    }
    if(change_flag)
    {
      Spd = nomal_speed + add_speed;
    }
    else
    {
      Spd = nomal_speed + add_speed;
    }
  }
}


