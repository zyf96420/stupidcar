#include "analysis.h"

uint8 Target_X = 0;
uint8 Target_Y = 0;
uint8 Target_X_Last = 0;
uint8 Target_X_Last_Last = 0;
uint8 Target_Y_Last = 0;
uint8 Target_Y_Last_Last = 0;
uint16 Target_Width = 0;
uint16 Target_Height = 0;
uint16 Target_Width_Last = 0;
uint16 Target_Height_Last = 0;

uint16 Target_Width_Old = 0;
uint16 Target_Width_Now = 0;

uint8 real_width = 0;
uint8 i,j;

void Image_Filter_Blur(void);

void Image_Filter(uint8 Type)
{
    switch(Type)
    {
    case 0:
    {
        Image_Filter_Blur();
        break;
    }
    default:break;
    }
}

void photo_unzip(uint8 *img)
{
    uint16 i=0;
    uint8 a=0,b=0;
    
    for(i=0 ;i<CAMERA_SIZE ;i++)   //将数组解压成80X60的数组
    {
       a=i/10;
       b=(i%10)<<3;
       photo[a][b+0]=(bool)((img[i] >> 7)& 0x01);
       photo[a][b+1]=(bool)((img[i] >> 6)& 0x01);
       photo[a][b+2]=(bool)((img[i] >> 5)& 0x01);
       photo[a][b+3]=(bool)((img[i] >> 4)& 0x01);
       photo[a][b+4]=(bool)((img[i] >> 3)& 0x01);
       photo[a][b+5]=(bool)((img[i] >> 2)& 0x01);
       photo[a][b+6]=(bool)((img[i] >> 1)& 0x01);
       photo[a][b+7]=(bool)((img[i] >> 0)& 0x01);
    }  
}

void photo_zip(uint8 *img)
{
   uint8 H_Index = 0;
   uint8 W_Index = 0;
   uint16 img_Index = 0;
   
    
   for(H_Index = 0;H_Index < OV7725_EAGLE_ARRAY_H;H_Index++ )
   {
     for(W_Index = 0;W_Index < OV7725_EAGLE_ARRAY_W;W_Index++ )
     {
       img_Index = H_Index * OV7725_EAGLE_ARRAY_W + W_Index;
       *(img + img_Index) |= (((uint8)photo[H_Index][(W_Index << 3) + 0]) << 7);
       *(img + img_Index) |= (((uint8)photo[H_Index][(W_Index << 3) + 1]) << 6);
       *(img + img_Index) |= (((uint8)photo[H_Index][(W_Index << 3) + 2]) << 5);
       *(img + img_Index) |= (((uint8)photo[H_Index][(W_Index << 3) + 3]) << 4);
       *(img + img_Index) |= (((uint8)photo[H_Index][(W_Index << 3) + 4]) << 3);
       *(img + img_Index) |= (((uint8)photo[H_Index][(W_Index << 3) + 5]) << 2);
       *(img + img_Index) |= (((uint8)photo[H_Index][(W_Index << 3) + 6]) << 1);
       *(img + img_Index) |= (((uint8)photo[H_Index][(W_Index << 3) + 7]) << 0);
     }
   }
}

void Image_Filter_Blur(void)
{
//  uint8 i = 0;
  uint8 H_Index = 0;
  uint8 W_Index = 0;
  
  for(H_Index = 1;H_Index < OV7725_EAGLE_H - 1;H_Index++)
  {
    for(W_Index = 1;W_Index < OV7725_EAGLE_W - 1;W_Index++)
    {
      if(!photo[H_Index][W_Index])
      {
        if((photo[H_Index][W_Index+1] && photo[H_Index][W_Index-1]) || (photo[H_Index+1][W_Index] && photo[H_Index-1][W_Index]))
        {
          photo[H_Index][W_Index] = 1;
        }
      }
    }
  }
}

uint8 Image_Search_Target(uint8 *img,uint8 *X,uint8 *Y,uint16 *Width,uint16 *Height)
{
  uint8 H_Index = 0;
  uint8 W_Index = 0; 
  uint16 H_Index_t = 0;
  uint16 W_SUM[OV7725_EAGLE_H] = {0};
  uint16 H_SUM[OV7725_EAGLE_ARRAY_W] = {0};
  
  uint8 W_MIN_INDEX = 0;
  uint8 H_MIN_INDEX = 0;
  uint16 H_MIN_VAL = 0;
//  uint16 W_MIN_VAL = 0;
  uint16 Temp_HW = 0;
  
  real_width = 0;
  i = 0;
  j = 0;
  
  for(H_Index = 0;H_Index < OV7725_EAGLE_H;H_Index++)
  {
    H_Index_t =  (uint16)H_Index * 10;
    W_SUM[H_Index] += *(img + H_Index_t + 0);
    W_SUM[H_Index] += *(img + H_Index_t + 1);
    W_SUM[H_Index] += *(img + H_Index_t + 2);
    W_SUM[H_Index] += *(img + H_Index_t + 3);
    W_SUM[H_Index] += *(img + H_Index_t + 4);
    W_SUM[H_Index] += *(img + H_Index_t + 5);
    W_SUM[H_Index] += *(img + H_Index_t + 6);
    W_SUM[H_Index] += *(img + H_Index_t + 7);
    W_SUM[H_Index] += *(img + H_Index_t + 8);
    W_SUM[H_Index] += *(img + H_Index_t + 9);
    
    H_SUM[0] += *(img + H_Index_t + 0);
    H_SUM[1] += *(img + H_Index_t + 1);
    H_SUM[2] += *(img + H_Index_t + 2);
    H_SUM[3] += *(img + H_Index_t + 3);
    H_SUM[4] += *(img + H_Index_t + 4);
    H_SUM[5] += *(img + H_Index_t + 5);
    H_SUM[6] += *(img + H_Index_t + 6);
    H_SUM[7] += *(img + H_Index_t + 7);
    H_SUM[8] += *(img + H_Index_t + 8);
    H_SUM[9] += *(img + H_Index_t + 9);
  }
  
  Temp_HW = 23333;
  for(H_Index = 8;H_Index < OV7725_EAGLE_H;H_Index++)//获得目标大致纵坐标
  {
    if(W_SUM[H_Index] < Temp_HW)
    {
        Temp_HW = W_SUM[H_Index];
        W_MIN_INDEX = H_Index;
  //      W_MIN_VAL = W_SUM[H_Index];
    }
  }
  
  H_Index_t = W_MIN_INDEX * 10;
  for(i=0;i<10;i++)
  {
    if(*(img + H_Index_t + i) == 255)
    {
      continue;
    }
    else
    {
      for(j=0;j<8;j++)
      {
        real_width += (bool)(~(*(img + H_Index_t + i)) & (0x01 << j));
      }
    }
  }
  
  Temp_HW = 23333;
  for(W_Index = 0;W_Index < OV7725_EAGLE_ARRAY_W;W_Index++)//获得目标大致横坐标
  {
    if(H_SUM[W_Index] < Temp_HW)
    {
      Temp_HW = H_SUM[W_Index];
      H_MIN_INDEX = W_Index;
      H_MIN_VAL = H_SUM[W_Index];
    }
  }
  
  if(H_MIN_VAL < 15200 && W_MIN_INDEX > 8)//2550,15300
  {
    *X = H_MIN_INDEX;
    *Y = W_MIN_INDEX;
//    Target_Width_Now = (2550 - W_MIN_VAL)/10;//255
//    if(Target_Width_Old != 0)
//      *Width = (Target_Width_Now + Target_Width_Old) >> 1;
//    else
//      *Width = Target_Width_Now;
//    Target_Width_Old =  *Width;
    *Height = (15300 - H_MIN_VAL)/60;
    
    *Width = real_width;
    return 0;
  }
  else
  {
    return 1;    
  }
}

void Get_Target_Offset(uint8 *Point_Old,uint8 *Point_Now,uint16 *Distance)
{
  uint16 temp = 0;
  uint16 temp2 = 0;
  if(*Point_Old > *Point_Now)
  {
    temp = (*Point_Old - *Point_Now)*(*Point_Old - *Point_Now);
  }
  else
  {
    temp = (*Point_Now - *Point_Old)*(*Point_Now - *Point_Old);
  }
  
  if(*(Point_Old + 1) > *(Point_Now + 1))
  {
    temp2 = (*(Point_Old + 1) - *(Point_Now + 1))*(*(Point_Old + 1) - *(Point_Now + 1));
  }
  else
  {
    temp2 = (*(Point_Now + 1) - *(Point_Old + 1))*(*(Point_Now + 1) - *(Point_Old + 1));
  }
  
  temp >>= 1;
  temp2 >>= 1;
  
  if((*Point_Old == 0) && (*(Point_Old + 1) == 0))
  {
    *Distance = 0;
  }
  if((*Point_Now == 0) && (*(Point_Now + 1) == 0))
  {
    *Distance = 0;
  }
  else
  {
    *Distance = temp + temp2;
  }
}

void Get_Target_Offset_Y(uint8 Point_Old_Y,uint8 Point_Now_Y,char *Distance)
{
  if(Point_Old_Y == 255 || Point_Now_Y == 255)
  {
    *Distance = 0;
  }
  else
  {
    *Distance = (char)Point_Old_Y - (char)Point_Now_Y;
  }
}

uint8 X_old = 255;
int32 tick_old = 0;
void Get_Car_Turn_Speed(uint8 *X,int32 *Speed)
{
    if(*X > 9)//未找到目标
    {
       *Speed = 0;
    }
    else if(X_old > 9)//目标第一次出现
    {
      *Speed = 0;
      tick_old = tick_10ms;
    }
    else if(X_old == *X)//若目标位置不动
    {
//      if(tick_10ms - tick_old > 20)
//        *Speed = 0;
      return;
    }
    else
    {
      *Speed = (((int32)*X - (int32)X_old)*1000)/(tick_10ms-tick_old);
      if(*Speed > 250 || *Speed < -250)
      {
        *Speed = 0;
      }
      tick_old = tick_10ms;
    }
  
    X_old = *X;
}
    