//////////////////////////////////////////////
//第十一届飞思卡尔智能车大赛
//江苏科技大学 极速涡流
//队    员：俞强 刘晓玺 
//指导老师：陈迅
//PID控制

#include "include.h"

extern int left_speed,right_speed;      //编码器的值
extern uint16 beacon_W;
extern uint8 beacon;

int speed_error=0,speed_error1=0,
    speed_error_left=0,
    speed_error_right=0,
    car_speed=0,car_speed1=0;

int SETPOINT_STEER=0;
//-----------------------电机PID参数-----------------------------
volatile double Kp1 = 6.0;                   //减小p参数可以减小超调3.0,6.0 9.8  Kp+Ki
volatile double Ki1 = 5.8;                   //增大i参数有利于减少调节时间，也可以减小超调量2.4,5.8 8.8    Kp
volatile double Kd1 = 0;

volatile double Kp2 = 9.8;                   //减小p参数可以减小超调3.0,6.0 9.8  Kp+Ki
volatile double Ki2 = 6.0;                   //增大i参数有利于减少调节时间，也可以减小超调量2.4,5.8 8.8    Kp
volatile double Kd2 = 0;
int incrementSpeed=0;

//-----------------------舵机PID参数-----------------------------
double p_steer1 = 1.2;
double i_steer1 = 0;
double d_steer1 = 0.5;

double p_steer2 = 1.6;  //2.0
double i_steer2 = 0;
double d_steer2 = 0.5;//0.5

//增量式PID
struct pid
{
    int SetSpeed;           //定义设定值
    int ActualSpeed;        //定义实际值 
    int err;
    int err_left;           //定义左轮偏差值(差速用)
    int err_right;          //定义右轮偏差值(差速用)
    int err_next;
    int err_next_left;      //定义左轮上一个偏差值(差速用)
    int err_next_right;     //定义右轮上一个偏差值(差速用)
    int err_last;
    int err_last_left;      //定义左轮最上前的偏差值(差速用)
    int err_last_right;     //定义右轮最上前的偏差值(差速用)
    float Kp1,Ki1,Kd1;      //定义比例、积分、微分系数
    float Kp2,Ki2,Kd2;      //定义比例、积分、微分系数
}pid;

//位置式PID,用于舵机
struct pid2
{
    int SetPoint;      			//设定目标Desired Value
    double Proportion1; 		//比例常数Proportional Const
    double Integral1; 			//积分常数Integral Const
    double Derivative1; 		//微分常数Derivative Const
    double Proportion2; 		//比例常数Proportional Const
    double Integral2; 			//积分常数Integral Const
    double Derivative2; 		//微分常数Derivative Const
    int LastError;    			//Error[-1]
    int PrevError;    			//Error[-2]
	int SumError;				//总误差
} pid2;

void Motor_PID_init()
{ 
  pid.SetSpeed=0;
  pid.ActualSpeed=0;
  pid.err=0;
  pid.err_left=0;
  pid.err_right=0;
  pid.err_last=0;
  pid.err_last_left=0;
  pid.err_last_right=0;
  pid.err_next=0;
  pid.err_next_left=0;
  pid.err_next_right=0;
  pid.Kp1=Kp1;
  pid.Ki1=Ki1;
  pid.Kd1=Kd1;
  pid.Kp2=Kp2;
  pid.Ki2=Ki2;
  pid.Kd2=Kd2;
}

void Steer_PID_init()
{
  	pid2.LastError = 0;       		            //Error[-1]
	pid2.PrevError = 0;       		            //Error[-2]
	pid2.SumError = 0;				            //总误差
	pid2.Proportion1 = p_steer1;  		        //比例常数Proportional Const
	pid2.Integral1 = i_steer1;    		        //积分常数Integral Const
	pid2.Derivative1 = d_steer1;  		        //微分常数Derivative Const
    pid2.Proportion2 = p_steer2;  		        //比例常数Proportional Const
	pid2.Integral2 = i_steer2;    		        //积分常数Integral Const
	pid2.Derivative2 = d_steer2;  		        //微分常数Derivative Const
	pid2.SetPoint = SETPOINT_STEER; 
}

int PID_speed(int Actualspeed,int Setspeed)
{
   pid.ActualSpeed = Actualspeed;
   pid.SetSpeed  = Setspeed;
   pid.err = pid.SetSpeed - pid.ActualSpeed;
   incrementSpeed =(int)(pid.Kp1*pid.err
                        -pid.Ki1*pid.err_next
                        +pid.Kd1*pid.err_last);
   pid.err_last = pid.err_next;
   pid.err_next = pid.err;
   return incrementSpeed;
}

int PID_speed_left(int Actualspeed,int Setspeed)
{
   pid.ActualSpeed = Actualspeed;
   pid.SetSpeed  = Setspeed;
   pid.err_left = pid.SetSpeed - pid.ActualSpeed;
   incrementSpeed =(int)(pid.Kp1*pid.err_left
                        -pid.Ki1*pid.err_next_left
                        +pid.Kd1*pid.err_last_left);
   pid.err_last_left = pid.err_next_left;
   pid.err_next_left = pid.err_left;
   return incrementSpeed;
}

int PID_speed_right(int Actualspeed,int Setspeed)
{
   pid.ActualSpeed = Actualspeed;
   pid.SetSpeed  = Setspeed;
   pid.err_right = pid.SetSpeed - pid.ActualSpeed;
   incrementSpeed =(int)(pid.Kp2*pid.err_right
                        -pid.Ki2*pid.err_next_right
                        +pid.Kd2*pid.err_last_right);
   pid.err_last_right = pid.err_next_right;
   pid.err_next_right = pid.err_right;
   return incrementSpeed;
}

int PID_Adjust(int Setspeed)
{
   static int pwm=0;
   car_speed = (left_speed+right_speed)/2;
   speed_error = PID_speed(car_speed,Setspeed);
   pwm  += speed_error;

   if(pwm>900)pwm=900;
   else if(pwm<0)pwm=0;
   return pwm;
}
int PID_Adjust_Left(int Setspeed)
{
   static int pwm_left=0;
   speed_error_left = PID_speed_left(left_speed,Setspeed);
   pwm_left  += speed_error_left;

   if(pwm_left>900)pwm_left=900;
   else if(pwm_left<0)pwm_left=0;
   return pwm_left;
}

int PID_Adjust_Right(int Setspeed)
{
   static int pwm_right=0;
   speed_error_right = PID_speed_right(right_speed,Setspeed);
   pwm_right  += speed_error_right;

   if(pwm_right>900)pwm_right=900;
   else if(pwm_right<0)pwm_right=0;
   return pwm_right;
}
