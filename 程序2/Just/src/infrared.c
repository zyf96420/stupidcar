//////////////////////////////////////////////
//第十一届飞思卡尔智能车大赛
//江苏科技大学 极速涡流
//队    员：俞强 刘晓玺 
//指导老师：陈迅
//红外避障函数

#include "infrared.h"

uint16 ad_value=0;
uint16 distance_left = 0;
uint16 distance_mid = 0;
uint16 distance_right = 0;

void infrared_init()
{
  adc_init(ADC0_SE10);              //ADC初始化 PTA7
  adc_init(ADC1_SE15);              //ADC初始化 PTB11
  adc_init(ADC1_SE17);              //ADC初始化 PTA17   
}

uint8 get_distance(void)
{                                    //AD转换后模拟电压
  ad_value=adc_once(ADC0_SE10,ADC_12bit);     //读AD值
  distance_right = 3300 - ((3300*ad_value)>>12);
  ad_value=adc_once(ADC1_SE15,ADC_12bit);     //读AD值
  distance_mid = 3300 - ((3300*ad_value)>>12);
  ad_value=adc_once(ADC1_SE17,ADC_12bit);     //读AD值
  distance_left = 3300 - ((3300*ad_value)>>12);
  return 0;
}